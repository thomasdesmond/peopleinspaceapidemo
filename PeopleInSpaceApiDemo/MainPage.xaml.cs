﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace PeopleInSpaceApiDemo
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void Handle_GetPeopleInSpace(object sender, System.EventArgs e)
        {
            var client = new HttpClient();

            var spaceApiAddress = "http://api.open-notify.org/astros.json";
            var uri = new Uri(spaceApiAddress);

            PeopleInSpaceModel spaceData = new PeopleInSpaceModel();
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                spaceData = JsonConvert.DeserializeObject<PeopleInSpaceModel>(jsonContent);
            }
            numberLabel.Text = "Number of people in space " + spaceData.Number.ToString();
            spaceListView.ItemsSource = new ObservableCollection<Person>(spaceData.People);
        }
    }
}
